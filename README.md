# Streamer Dashboard ATEM Connector

[![pipeline status](https://gitlab.com/churchtechhelp/streaming-dashboard/relay-atem/badges/master/pipeline.svg)](https://gitlab.com/churchtechhelp/streaming-dashboard/relay-atem/-/commits/master)

This Node.js app bridges the ATEM (UDP) and the Streamer Dashboard (Websocket).

This project implements the ATEM protocol in a maintainable way for the dashboard.
Other libraries were either incomplete, their current release did not work, or they were in another language.

This project is licensed under the GNU GPL v3.0

## Installation/Usage

Please grab the latest build artifact for the latest version.

## Attribution

This project has referenced several other projects to understand the ATEM protocol.

https://www.skaarhoj.com/fileadmin/BMDPROTOCOL.html
https://github.com/nrkno/tv-automation-atem-connection
https://github.com/petersimonsson/libqatemcontrol
