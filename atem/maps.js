function resolveVideoSourceName(inputNumber){
    const inputNumberStr = inputNumber.toString();
    if(inputNumber == 0) return "Black";
    if(inputNumber < 1000) return "Input " + inputNumber;
    if(inputNumber < 2000) return "Color Bars";
    if(inputNumber < 3000) return "Color " + (inputNumber - 2000);
    if(inputNumber < 4000) return "Media Player " + inputNumberStr[2] + ((inputNumberStr[3] == '1')? " Key":"");
    if(inputNumber < 5000) return "Key " + inputNumberStr[2] + " Mask";
    if(inputNumber < 6000) return "DSK " + inputNumberStr[2] + " Mask";
    if(inputNumber < 7000) return "Super Source";
    if(inputNumber < 8000) return "Clean Feed " + (inputNumber - 7000);
    if(inputNumber < 9000) return "Auxilary " + (inputNumber - 8000);
    if(inputNumber > 10000 && inputNumber < 11000) return "Media Player " + inputNumberStr[3] + ((inputNumberStr[4] == '1')? " Prev":" Prog");
}

function resolveAudioSourceName(inputNumber){
    if(inputNumber < 1000) return "Input " + inputNumber;
    if(inputNumber == 1001) return "XLR";
    if(inputNumber == 1101) return "AES/EBU";
    if(inputNumber == 1201) return "RCA";
    if(inputNumber > 2000 && inputNumber < 3000) return "MP" + inputNumber.toString()[3];
}

const audioTypeMap = [
    "External Video",
    "Media Player",
    "External Audio",
];

const audioPlugMap = {
      0: "Internal",
      1: "SDI",
      2: "HDMI",
      3: "Component",
      4: "Composite",
      5: "SVideo",
     32: "XLR",
    128: "RCA",
};

const audioMixMap = [
    "Off",
    "On",
    "AFV",
];

module.exports = {
    resolveAudioSourceName,
    resolveVideoSourceName,
    audioTypeMap,
    audioPlugMap,
    audioMixMap,
}