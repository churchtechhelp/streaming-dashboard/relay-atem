class PreparerException extends Error {
    constructor(message){
        super(message);
        this.name = "PreparerException"
    }
}

module.exports = PreparerException;