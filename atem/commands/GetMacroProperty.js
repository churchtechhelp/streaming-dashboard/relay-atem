module.exports = {
    name: "MacroProperties",
    command: "MPrp",
    length: -1,
    description: "Recieves a macro's properties",
    type: "GET",
    process(data){
        const index      = data.readUInt16BE(0);
        const nameLength = data.readUInt16BE(4);
        const descLength = data.readUInt16BE(6);

        let macro = {};

        if(typeof this.state.macros[index] !== 'undefined') {
            macro = this.state.macros[index];
        } 

        macro.isUsed = data.readUInt8(2) == 1;
        macro.name = data.toString('ascii',8,8+nameLength);
        macro.description = data.toString('ascii',8+nameLength,8+nameLength+descLength);

        this.state.macros[index] = macro;
    },
    state(){
        return {
            macros: []
        }
    }
}