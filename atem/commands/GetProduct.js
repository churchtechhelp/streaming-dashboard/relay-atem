module.exports = {
    name: 'Product',
    description: 'Gets the Switcher\'s Product Name',
    type: 'GET',
    command: '_pin',
    length: 44, // Bytes
    version: '1.0',
    // No Max Version
    process(data) {
        // Take the 44 bytes and convert to string
        let end = data.indexOf(0);
        if(end < 0 || end > 44) end = 44;
        this.state.product = data.toString('ascii',0,end);
    },
    state(){
        return {
            product: "",
        }
    }
}