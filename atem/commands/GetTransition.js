module.exports = {
    name: "GetTransition",
    command: "TrSS",
    type: "GET",
    length: 8,
    description: "Gets the type of transition, including the keyers",
    me: true,
    process(data){
        const me = data.readUInt8(0);
        this.state.me[me].currentStyle = data.readUInt8(1);
        const keyers = data.readUInt8(2);
        // Requires keyerCount to be set already!
        for(let i = 0; i < this.state.me[me].keyerCount; i++){
            this.state.me[me].currentKeyers[i] = ((keyers << i) & 1) == 1;
        }
        this.state.me[me].nextStyle = data.readUInt8(3);
        const nextKeyers  = data.readUInt8(4);
        for(let i = 0; i < this.state.me[me].keyerCount; i++){
            this.state.me[me].nextKeyers[i] = ((nextKeyers << i) & 1) == 1;
        }
    },
    state(){
        return {
            currentStyle: 0,
            currentKeyers: [ true, null, null, null, null ],
            nextStyle: 0,
            nextKeyers: [ true, null, null, null, null ],
        }
    }
}