module.exports = {
    name: "FileTransferDone",
    command: "FTDC",
    type: "GET",
    description: "File Transfer Data Complete",
    process(data){
        const transferId = data.readUInt16BE();
        console.log("File tranfer complete",transferId);
        // Maybe do something with current transfer buffer?
    }
}