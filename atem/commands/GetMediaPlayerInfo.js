module.exports = {
    name: 'MediaPlayerInfo',
    command: '_mpl',
    type: "GET",
    length: 4,
    process(data){
        this.state.mediaPlayerStills = data.readUInt8(0);
        this.state.mediaPlayerClips = data.readUInt8(1);
    },
    state(){
        return {
            mediaPlayerStills: 0,
            mediaPlayerClips: 0,
        }
    }
}