module.exports = {
    name: "SetTransitionPosition",
    command: "CTPs",
    type: "SET",
    length: 4,
    description: "Sets the position of the transition in an M/E on a scale from 0-10000. The transition will end at 0 (or 10000+ for this library)",
    prepare(data){
        if(!('me' in data)) data.me = 0;
        let buffer = Buffer.alloc(4);
        buffer.writeUInt8(data.me,0);
        if(data.position >= 10000) data.position = 0;
        buffer.writeUInt16BE(data.position,2);
        return buffer;
    }
}