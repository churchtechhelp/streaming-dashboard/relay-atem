module.exports = {
    name: "GetDownstreamKeyerInputs",
    command: "DskB",
    type: "GET",
    length: 8,
    description: "Gets the Downstream Keyer input mappings",
    process(data){
        const keyer = data.readUInt8();
        if(this.state.downstreamKeyers[keyer] === undefined){
            this.state.downstreamKeyers[keyer] = {};
        }
        this.state.downstreamKeyers[keyer]["fillInput"] = data.readUInt16BE(2);
        this.state.downstreamKeyers[keyer]["keyInput"] = data.readUInt16BE(4);
    },
    state(){
        return {
            downstreamKeyers: [
                {
                    fillInput: 0,
                    keyInput: 0,
                },
                {
                    fillInput: 0,
                    keyInput: 0,
                }
            ]
        }
    }
}