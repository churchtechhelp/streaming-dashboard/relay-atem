module.exports = {
    name: "MixEffectConfig",
    command: "_MeC",
    type: "GET",
    length: 4,
    description: "Gives the number of upstream keys",
    me: true,
    process(data){
        const me = data.readUInt8(0);
        this.state.me[me].keyerCount = data.readUInt8(1);
    },
    state(){
        return {
            keyerCount: 0,
        };
    }
}