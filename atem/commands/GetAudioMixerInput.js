const { audioTypeMap, audioPlugMap, audioMixMap, resolveAudioSourceName } = require('../maps.js');

module.exports = {
    name: "GetAudioMixerInput",
    command: "AMIP",
    type: "GET",
    length: 16,
    description: "Audio Mixer Input descriptions",
    process(data){
        const source = data.readUInt16BE(0);
        this.state.audioMixer.inputs[source] = {
            name: resolveAudioSourceName(source),
            type: audioTypeMap[data.readUInt8(2)],
            mediaPlayer: data.readUInt8(6) > 0,
            plug: audioPlugMap[data.readUInt8(7)],
            mix: audioMixMap[data.readUInt8(8)],
            volume: data.readUInt16BE(10),
            balance: data.readInt16BE(12),
        }
    },
    state(){
        return {
            audioMixer: {
                inputs: []
            }
        }
    }
}