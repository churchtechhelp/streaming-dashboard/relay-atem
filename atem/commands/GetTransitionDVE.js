module.exports = {
    name: "GetTransitionDVE",
    command: "TDvP",
    type: "GET",
    length: 20,
    description: "Gets the settings for the DVE transition",
    me: true,
    process(data){
        const me = data.readUInt8(0);
        if(me > 1) {
            console.log("TDvP: ME Error");
            return;
        }
        this.state.me[me].transitions.dve.rate          = data.readUInt8(1);
        this.state.me[me].transitions.dve.logoRate      = data.readUInt8(2);
        this.state.me[me].transitions.dve.style         = data.readUInt8(3);
        this.state.me[me].transitions.dve.fill          = data.readUInt16BE(4);
        this.state.me[me].transitions.dve.key           = data.readUInt16BE(6);
        this.state.me[me].transitions.dve.enable        = data.readUInt8(8) > 0;
        this.state.me[me].transitions.dve.premultiplied = data.readUInt8(9) > 0;
        this.state.me[me].transitions.dve.clip          = data.readUInt16BE(10);
        this.state.me[me].transitions.dve.gain          = data.readUInt16BE(12);
        this.state.me[me].transitions.dve.invert        = data.readUInt8(14) > 0;
        this.state.me[me].transitions.dve.reverse       = data.readUInt8(15) > 0;
        this.state.me[me].transitions.dve.flipflop      = data.readUInt8(16) > 0;
    },
    state(){
        return {
            transitions: {
                dve: {
                    rate: 0,
                    logoRate: 0,
                    style: 0,
                    fill: 0,
                    key: 0,
                    enable: 0,
                    premultiplied: 0,
                    clip: 0,
                    gain: 0,
                    invert: 0,
                    reverse: 0,
                    flipflop: 0,
                },
            },
        };
    },
}