//import { }

module.exports = {
    name: "InputProps",
    description: "Input Properties",
    command: "InPr",
    type: "GET",
    length: 36,
    process(data){
        const inputNumber = data.readUInt16BE();
        if(this.state.inputs[inputNumber] == undefined) this.state.inputs[inputNumber] = {};
        // Long Name (2-21 string)
        let stringLength = data.indexOf(0,2) - 2;
        if(stringLength < 0 || stringLength > 20) stringLength = 20;
        this.state.inputs[inputNumber]["longName"] =  data.toString('ascii',2,2+stringLength);
        // Short Name (22-25 string)
        stringLength = data.indexOf(0,22) - 22;
        if(stringLength < 0 || stringLength > 5) stringLength = 5;
        this.state.inputs[inputNumber]["shortName"] = data.toString('ascii',22,22+stringLength);
        // Port Types Available (27: 5 bits: SDI, HDMI, Component, Composite, SVideo)
        const portTypeByte = data.readUInt8(27);
        let portTypesAvailable = [];
        if(portTypeByte & 0x01 == 1) portTypesAvailable.push("SDI");
        if(portTypeByte & 0x02 == 2) portTypesAvailable.push("HDMI");
        if(portTypeByte & 0x04 == 4) portTypesAvailable.push("Component");
        if(portTypeByte & 0x08 == 8) portTypesAvailable.push("Composite");
        if(portTypeByte & 0x10 == 16) portTypesAvailable.push("SVideo");
        this.state.inputs[inputNumber]['portTypesAvailable'] = portTypesAvailable;
        // 29: External Port Type Used
        const externalPortTypeMap = [
            "Internal",
            "SDI",
            "HDMI",
            "Composite",
            "Component",
            "SVideo",
        ]
        let externalPortTypeUsed = data.readUInt8(29);
        if(externalPortTypeUsed <= externalPortTypeMap.length) {
            this.state.inputs[inputNumber]['externalPortTypeUsed'] = externalPortTypeMap[externalPortTypeUsed];
        }
        // 30: Port Type (External, black, color bars, color generator, mP Fill, MP Key, SuperSOurce, ME Out, Aux, Mask)
        const portTypeMap = {
            0: "External",
            1: "Black",
            2: "Color Bars",
            3: "Color Generator",
            4: "Media Player Fill",
            5: "Media Player Key",
            6: "SuperSource",
            128: "ME Output",
            129: "Auxilary",
            130: "Mask",
        }
        let portType = data.readUInt8(30);
        if(portType in portTypeMap) this.state.inputs[inputNumber]['portType'] = portTypeMap[portType];
        // 32: Availibility (5 bits: Aux, Multiview, super source art, super source box, key sources)
        // 33: ME Availability (2 bits for each ME)
        // 34: Available for Unknown
    },
    state(){
        return {
            inputs: {}
        }
    }
}