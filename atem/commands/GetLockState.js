module.exports = {
    name: "GetLockState",
    command: "LKST",
    type: "GET",
    length: 4,
    description: "Lock State of ???",
    process(data){
        this.state.locks[data.readUInt8(0)] = data.readUInt8(2) > 1;
    },
    state(){
        return {
            locks: [],
        };
    },
}