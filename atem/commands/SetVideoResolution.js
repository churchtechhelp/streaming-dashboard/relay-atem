const Buffer = require('buffer');

const videoModeMap = [
    "525i59.94",
    "625i50",
    "525i59.94",
    "625i50",
    "720p50",
    "720p59.94",
    "1080i59.94",
    "1080p23.98",
    "1080p24",
    "1080p25",
    "1080p29.97",
    "1080p50",
    "1080p59.97",
    "1080i50",
    "2160p23.98",
    "2160p24",
    "2160p25",
    "2160p29.97",
]

module.exports = {
    name: 'SetVideoResolution',
    command: 'CVdM',
    type: "PUT",
    length: 4,
    process(data){
        const mode = data.readUInt8(0);
        if (mode > 17) return;
        this.state.resolution = videoModeMap[mode];
    },
    prepare(resolution){
        if(typeof resolution == "string") {
            // Need to convert to number
            resolution = videoModeMap.indexOf(resolution);
            if(resolution < 0) return;
        }
        let buffer = Buffer.alloc(4);
        buffer.writeUint8(resolution,0);
        return buffer;
    }
}