const PreparerException = require('../PreparerException.js');

module.exports = {
    name: "SetDSKTie",
    command: "CDsT",
    length: 4,
    type: "SET",
    description: "Ties/Unties the given downstream keyer",
    prepare(data){
        if(!('keyer' in data)) {
            throw new PreparerException("Keyer is required.");
        }
        if(typeof data.keyer != 'number') {
            throw  new PreparerException("Keyer has incorrect type: " + typeof data.keyer);
        }
        if(!('tied' in data)) {
            throw new PreparerException("Tied is required.");
        }
        if(typeof data.tied != 'boolean' && typeof data.tied != 'number') {
            throw  new PreparerException("Tied has incorrect type: " + typeof data.tied);
        }
        let buffer = Buffer.alloc(4);
        buffer.writeUInt8(data.keyer,0);
        if(data.tied){
            buffer.writeUInt8(1,1);
        }
        return buffer;
    }
}