module.exports = {
    name: "GetTransitionWipe",
    command: "TWpP",
    type: "GET",
    length: 20,
    description: "Gets the settings for the wipe transition",
    me: true,
    process(data){
        const me = data.readUInt8(0);
        let settings = this.state.me[me].transitions.wipe;
        settings.rate = data.readUInt8(1);
        settings.pattern = data.readUInt8(2);
        settings.width = data.readUInt16BE(4);
        settings.fill = data.readUInt16BE(6);
        settings.symmetry = data.readUInt16BE(8);
        settings.softness = data.readUInt16BE(10);
        settings.x = data.readUInt16BE(12);
        settings.y = data.readUInt16BE(14);
        settings.reverse = data.readUInt8(16) > 0;
        settings.flipflop = data.readUInt8(17) > 0;
        this.state.me[me].transitions.wipe = settings;
    },
    state(){
        return {
            transitions: {
                wipe: {
                    rate: 0,
                    pattern: 0,
                    width: 0,
                    fill: 0,
                    symmetry: 50,
                    softness: 0,
                    x: 5000,
                    y: 5000,
                    reverse: false,
                    flipflop: false,
                },
            },
        };
    },
}