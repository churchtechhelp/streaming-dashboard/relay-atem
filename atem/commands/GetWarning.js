module.exports = {
    name: 'Warning',
    description: 'Gets whatever warning there is currently',
    type: 'GET',
    command: 'Warn',
    length: 44, // Bytes
    version: '1.0',
    // No Max Version
    watch: true,
    process(data) {
        // Take the 44 bytes and convert to string
        const end = data.indexOf(0);
        this.state.warning = data.toString(0,end);
    },
    state(){
        return {
            warning: "",
        }
    }
}