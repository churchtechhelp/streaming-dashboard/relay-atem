module.exports = {
    name: "GetDownstreamKeyerStatus",
    command: "DskS",
    type: "GET",
    length: 8,
    description: "Gets the Downstream Keyer transition status",
    process(data){
        const keyer = data.readUInt8();
        if(this.state.downstreamKeyers[keyer] === undefined){
            this.state.downstreamKeyers[keyer] = {};
        }
        this.state.downstreamKeyers[keyer]["live"] = data.readUInt8(1) > 0;
        this.state.downstreamKeyers[keyer]["transitioning"] = data.readUInt8(2) > 0;
        this.state.downstreamKeyers[keyer]["autoTransitioning"] = data.readUInt8(3) > 0;
        this.state.downstreamKeyers[keyer]["autoFramesRemaining"] = data.readUInt8(4);
    },
    state(){
        return {
            downstreamKeyers: [
                {
                    "live": false,
                    "transitioning":false,
                    "autoTransitioning":false,
                    "autoFramesRemaining":false,
                },
                {
                    "live": false,
                    "transitioning":false,
                    "autoTransitioning":false,
                    "autoFramesRemaining":false,
                },
            ]
        }
    }
}