module.exports = {
    name: 'Topology',
    description: 'Gets the abilities of the switcher',
    type: 'GET',
    command: '_top',
    length: 12, // Bytes
    version: '1.0',
    // No Max Version
    process(data) {
        // Number of ME
        this.state.topology.me = data.readUInt8(0);
        // Sources
        this.state.topology.sources = data.readUInt8(1);
        // Color Generators
        this.state.topology.colors = data.readUInt8(2);
        // Aux buses
        this.state.topology.aux = data.readUInt8(3);
        // downstream keys
        this.state.topology.downstream = data.readUInt8(4);
        // stingers
        this.state.topology.stingers = data.readUInt8(5);
        // dve
        this.state.topology.dve = data.readUInt8(6);
        // superSources
        this.state.topology.super = data.readUInt8(7);
        // RESERVED
        // Bit 0 = SD output
        this.state.topology.sd = (data.readUInt8(9) && 1) == 1
        // RESERVED
        // RESERVED
    },
    state(){
        return {
            topology: {
                me: 0,
                sources: 0,
                colors: 0,
                aux: 0,
                downstream: 0,
                stingers: 0,
                dve: 0,
                super: 0,
                sd: false,
            }
        }
    }
}