const PreparerException = require('../PreparerException.js');
module.exports = {
    name: "SetDSKTime",
    command: "CDsR",
    length: 4,
    type: "SET",
    description: "Sets the auto frames the given downstream keyer",
    prepare(data){
        if(!('keyer' in data)) {
            throw new PreparerException("Keyer is required.");
        }
        if(typeof data.keyer != 'number') {
            throw  new PreparerException("Keyer has incorrect type: " + typeof data.keyer);
        }
        if(!('frames' in data)) {
            throw new PreparerException("Frames is required.");
        }
        if(typeof data.frames != 'number') {
            throw  new PreparerException("Frames has incorrect type: " + typeof data.frames);
        }
        let buffer = Buffer.alloc(4);
        buffer.writeUInt8(data.keyer,0);
        buffer.writeUInt8(data.frames,1);
        return buffer;
    }
}