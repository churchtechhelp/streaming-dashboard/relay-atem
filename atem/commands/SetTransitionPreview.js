module.exports = {
    name: "SetTransitionPreview",
    command: "CTPr",
    length: 4,
    type: "SET",
    description: "Turns on/off the transition preview mode on an M/E",
    prepare(data){
        if(!('me' in data)) data.me = 0;
        let buffer = Buffer.alloc(4);
        buffer.writeUInt8(data.me,0);
        if('preview' in data && data.preview === true){
            buffer.writeUInt8(1,1);
        }
        return buffer;
    },
}