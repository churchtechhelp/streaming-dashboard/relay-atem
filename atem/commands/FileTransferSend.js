module.exports = {
    name: "FileTransferSend",
    command: 'FTDa',
    length: -1, // Variable
    description: "Sends/Recieves file transfer data",
    type: "GET",
    process(data){

    },
    prepare(transferId,data){
        // Assumption: Data is already an array that came in from websocket. Conversion should have been done on browser.
        const length = data.length + 4;
        data = Buffer.from(data);
        const buffer = Buffer.alloc(length);
        buffer.writeUInt16BE(transferId, 0);
        buffer.writeUInt16BE(data.length, 2);
        buffer.copy(data,4);

        return buffer;
    }
}