module.exports = {
    name: "GetTransitionDip",
    command: "TDpP",
    type: "GET",
    length: 4,
    description: "Gets the settings for the dip transition",
    me: true,
    process(data){
        const me = data.readUInt8(0);
        this.state.me[me].transitions.dip.rate = data.readUInt8(1);
        this.state.me[me].transitions.dip.source = data.readUInt16BE(2);
    },
    state(){
        return {
            transitions: {
                dip: {
                    rate: 0,
                    source: 0,
                },
            },
        };
    },
}