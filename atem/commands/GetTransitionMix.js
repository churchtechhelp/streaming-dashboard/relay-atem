module.exports = {
    name: "GetTransitionMix",
    command: "TMxP",
    type: "GET",
    length: 4,
    description: "Gets the settings for the mix transition",
    me: true,
    process(data){
        const me = data.readUInt8(0);
        this.state.me[me].transitions.mix = data.readUInt8(1);
    },
    state(){
        return {
            transitions: {
                mix: 0,
            },
        };
    },
}