// https://github.com/petersimonsson/libqatemcontrol/blob/252b0319fabf291a90d791bddc7619c2ebf0b095/qatemconnection.cpp Line 1551
module.exports = {
    name: 'AcceptData',
    description: 'Tells the switcher that the data was acked?',
    type: 'GET',
    command: 'FTUA',
    length: 4, // Bytes of data
    version: '1.0',
    // No Max Version
    process(state,data) {
        // Insufficient documentation
        // Guessing that the 4 bytes point to some media player image?
    },
}