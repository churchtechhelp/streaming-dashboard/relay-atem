module.exports = {
    name: "TransitionPreview",
    command: "TrPr",
    type: "GET",
    length: 4,
    description: "Boolean for if the switcher is in transition preview mode",
    me: true,
    process(data){
        const me = data.readUInt8(0);
        this.state.me[me].transitionPreview = data.readUInt8(1) == 1;
    },
    state(){
        return {
            transitionPreview: false,
        }
    }
}