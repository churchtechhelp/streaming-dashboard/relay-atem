module.exports = {
    name: "CameraControl",
    command: "CCdP",
    length: 24,
    type: "GET",
    description: "Returns camera control information.",
    process(data){
        const input = data.readUInt8(0);
        let cameraData = this.state.cameras[input];
        const adjType = data.readUInt8(1); // 0 = lens, 1 = camera, 8 = chip
        const adjFeature = data.readUInt8(2); // Varies based on adjType
        const adj1 = data.readUInt16BE(16);
        // Process it
        if(adjType == 0 && adjFeature == 3) { //Iris
            cameraData['iris'] = adj1;
        } else if(adjType == 0 && adjFeature == 0) { // Focus
            cameraData['focus'] = adj1;
        }

        if(adjType > 0){
            const adj2 = data.readUInt16BE(18);
            
            
        }
        if(adjType > 7 && adjFeature < 3){
            const adj3 = data.readUInt16BE(20);
            const adj4 = data.readUInt16BE(22);
        }
    },
    state(){
        return {
            cameras: {
                0:{},
                1:{},
                2:{},
                3:{},
                4:{},
                5:{},
                6:{},
                7:{},
                8:{},
            }
        }
    }
}