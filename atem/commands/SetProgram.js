module.exports = {
    name: "SetProgramInput",
    type: "SET",
    command: "CPgI",
    description: "Sets the Program Input",
    length: 4,
    process(data){
        // For simulator
        this.state.programIn = data;
    },
    prepare(inputNumber){
        if(!('me' in data)) data.me = 0;
        if(typeof data.input == 'string'){
            if (isNaN(data.input)) {
                console.log("Error: SetProgram does not yet accept strings for input. Recieved", data.input);
                return;
            } else {
                data.input = parseInt(data.input);
            }
        }
        let buffer = Buffer.alloc(4);
        buffer.writeUInt8(data.me,0);
        buffer.writeUInt16BE(data.input,2);
        return buffer;
    }
}