module.exports = {
    name: "SetTransitionMix",
    command: "CTMx",
    type: "SET",
    length: 4,
    description: "Sets the rate for the mix transition",
    prepare(data){
        if(!('me' in data)) data.me = 0;
        let buffer = Buffer.alloc(4);
        buffer.writeUInt8(data.me,0);
        buffer.writeUInt8(data.rate,1);
        return buffer;
    },
}