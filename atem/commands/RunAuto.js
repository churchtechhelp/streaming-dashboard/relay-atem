module.exports = {
    name: "RunAuto",
    command: "DAut",
    type: "SET",
    length: 4,
    description: "Clicks the Auto button, running whatever transition that is there",
    prepare(data){
        let buffer = Buffer.alloc(4);
        if('me' in data) buffer.writeUInt8(data.me,0);
        return buffer;
    }
}