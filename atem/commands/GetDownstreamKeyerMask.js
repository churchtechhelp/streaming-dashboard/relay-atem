module.exports = {
    name: "GetDownstreamKeyerMask",
    command: "DskP",
    type: "GET",
    length: 8,
    description: "Gets the Downstream Keyer mask settings",
    process(data){
        const keyer = data.readUInt8();
        if(this.state.downstreamKeyers[keyer] === undefined){
            this.state.downstreamKeyers[keyer] = {};
        }
        this.state.downstreamKeyers[keyer]["tied"] = data.readUInt8(1) > 0;
        this.state.downstreamKeyers[keyer]["autoFrames"] = data.readUInt8(2);
        this.state.downstreamKeyers[keyer]["premultiplied"] = data.readUInt8(3) > 0;
        this.state.downstreamKeyers[keyer]["clip"] = data.readUInt16BE(4);
        this.state.downstreamKeyers[keyer]["gain"] = data.readUInt16BE(6);
        this.state.downstreamKeyers[keyer]["invertedKey"] = data.readUInt8(8) > 0;
        this.state.downstreamKeyers[keyer]["masked"] = data.readUInt8(9) > 0;
        this.state.downstreamKeyers[keyer]["top"] = data.readInt16BE(10);
        this.state.downstreamKeyers[keyer]["bottom"] = data.readInt16BE(12);
        this.state.downstreamKeyers[keyer]["left"] = data.readInt16BE(14);
        this.state.downstreamKeyers[keyer]["right"] = data.readInt16BE(16);
    },
    state(){
        return {
            downstreamKeyers: [
                {
                    tied: false,
                    autoFrames: 0,
                    premultiplied: false,
                    clip: 0,
                    gain: 0,
                    invertedKey: false,
                    masked: false,
                    top: 0,
                    bottom: 0,
                    left: 0,
                    right: 0,
                },
                {
                    tied: false,
                    autoFrames: 0,
                    premultiplied: false,
                    clip: 0,
                    gain: 0,
                    invertedKey: false,
                    masked: false,
                    top: 0,
                    bottom: 0,
                    left: 0,
                    right: 0,
                },
            ]
        }
    }
}