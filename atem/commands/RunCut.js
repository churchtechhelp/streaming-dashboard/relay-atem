module.exports = {
    name: "RunCut",
    command: "DCut",
    type: "SET",
    length: 4,
    description: "Clicks the Cut button, swapping preview/program instantly",
    prepare(data){
        let buffer = Buffer.alloc(4);
        if('me' in data) buffer.writeUInt8(data.me,0);
        return buffer;
    }
}