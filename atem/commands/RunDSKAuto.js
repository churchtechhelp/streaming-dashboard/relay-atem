module.exports = {
    name: "RunDSKAuto",
    command: "DDsA",
    length: 4,
    type: "SET",
    description: "Triggers an auto fade for the given downstream keyer",
    prepare(data){
        let buffer = Buffer.alloc(4);
        buffer.writeUInt8(data.keyer,0);
        return buffer;
    }
}