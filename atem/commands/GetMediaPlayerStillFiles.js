module.exports = {
    name: "GetMediaPlayerFiles",
    command: "MPfe",
    length: -1,
    type: "GET",
    description: "FIle name for Media Player files",
    process(data){
        const pool = data.readUInt8(0); // or media pool
        const bank = data.readUInt16BE(2);
        const used = data.readUInt8(4) > 0;
        const hash = data.toString('base64',5,16);
        const nameLength = data.readUInt8(23);
        const name = data.toString('ascii',24,24+nameLength);
        if(this.state.mediaPlayer.stillFiles[bank] === undefined){
            this.state.mediaPlayer.stillFiles[bank] = {};
        }
        if(pool == 0){
            this.state.mediaPlayer.stillFiles[bank].hash = hash;
            this.state.mediaPlayer.stillFiles[bank].used = used;
            this.state.mediaPlayer.stillFiles[bank].name = name;
        } else {
            console.log("Media Player file unknown type",type,"Name",name);
        }
    },
    state(){
        return {
            mediaPlayer: {
                stillFiles: [],
            },
        };
    }
}