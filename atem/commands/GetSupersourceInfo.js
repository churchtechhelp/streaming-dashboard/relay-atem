module.exports = {
    name: 'SuperSourceInfo',
    command: '_m_SSCpl',
    type: "GET",
    length: 4,
    process(data){
        this.state.supersourceBoxes = data.readUInt8(0);
    },
    state(){
        return {
            supersourceBoxes: 0,
        }
    }
}