module.exports = {
    name: 'AudioChannelInfo',
    command: '_AMC',
    type: "GET",
    length: 4,
    process(data){
        this.state.audio.channelCount = data.readUInt8(0);
        this.state.audio.hasMonitor = (data.readUInt8(1) & 1) == 1;
    },
    state(){
        return {
            audio: {
                channelCount: 0,
                hasMonitor: false,
            }
        }
    }
}