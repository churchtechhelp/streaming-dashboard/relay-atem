module.exports = {
    name: 'VideoMixerInfo',
    command: '_VMC',
    type: "GET",
    length: 4,
    process(data){
        const bytes = data.readUIntBE(1,3);
        if(bytes & 1) this.state.supportedResolutions.push("525i59.94");      // 0 NTSC
        if(bytes & 2) this.state.supportedResolutions.push("625i50");         // 1 PAL
        if(bytes & 4) this.state.supportedResolutions.push("525i59.94");      // 2 NTSC
        if(bytes & 8) this.state.supportedResolutions.push("625i50");         // 3 PAL
        if(bytes & 16) this.state.supportedResolutions.push("720p50");        // 4
        if(bytes & 32) this.state.supportedResolutions.push("720p59.94");     // 5
        if(bytes & 64) this.state.supportedResolutions.push("1080i59.94");    // 6
        if(bytes & 128) this.state.supportedResolutions.push("1080p23.98");   // 7
        if(bytes & 256) this.state.supportedResolutions.push("1080p24");      // 8
        if(bytes & 512) this.state.supportedResolutions.push("1080p25");      // 9
        if(bytes & 1024) this.state.supportedResolutions.push("1080p29.97");  // 10
        if(bytes & 2048) this.state.supportedResolutions.push("1080p50");     // 11
        if(bytes & 4096) this.state.supportedResolutions.push("1080p59.97");  // 12
        if(bytes & 8192) this.state.supportedResolutions.push("1080i50");     // 13
        if(bytes & 0x4000) this.state.supportedResolutions.push("2160p23.98") // 14
        if(bytes & 0x8000) this.state.supportedResolutions.push("2160p24")    // 15
        if(bytes & 0x10000) this.state.supportedResolutions.push("2160p25")   // 16
        if(bytes & 0x20000) this.state.supportedResolutions.push("2160p29.97")// 17
    },
    state(){
        return {
            supportedResolutions: [],
        }
    }
}