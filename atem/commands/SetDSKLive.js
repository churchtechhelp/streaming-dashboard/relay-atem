module.exports = {
    name: "SetDSKLive",
    command: "CDsL",
    length: 4,
    type: "SET",
    description: "Makes the given downstream keyer live or hidden",
    prepare(data){
        let buffer = Buffer.alloc(4);
        buffer.writeUInt8(data.keyer,0);
        if(data.live){
            buffer.writeUInt8(1,1);
        }
        return buffer;
    }
}