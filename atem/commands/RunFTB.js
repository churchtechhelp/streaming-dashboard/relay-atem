module.exports = {
    name: "RunFTB",
    command: "FtbA",
    type: "SET",
    length: 4,
    description: "Clicks the FTB button",
    prepare(data){
        let buffer = Buffer.alloc(4);
        if('me' in data) buffer.writeUInt8(data.me,0);
        buffer.writeUInt8(2); // Confirmed
        return buffer;
    }
}