module.exports = {
    name: "ProgramInput",
    command: "PrgI",
    length: 4,
    type: "GET",
    me: true,
    process(data) {
        const me = data.readUInt8(0);
        this.state.me[me].programIn = data.readUInt16BE(2);
    },
    state(){
        return {
            programIn: 0,
        }
    }
}