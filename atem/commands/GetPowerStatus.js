module.exports = {
    name: "GetPowerStatus",
    command: "Powr",
    type: "GET",
    length: 4,
    description: "Gets the power state",
    process(data){
        const status = data.readUInt8(0);
        this.state.mainPower = status & 1 == 1;
        this.state.backupPower = status & 2 == 2;
    },
    state(){
        return {
            mainPower: false,
            backupPower: false,
        }
    }
}