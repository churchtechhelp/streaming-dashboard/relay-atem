module.exports = {
    name: "SetTransition",
    type: "SET",
    command: "CTTp",
    description: "Sets the current/next transition",
    length: 4,
    process(data){
        // For simulator
    },
    prepare(data){
        if(!('me' in data)) data.me = 0;
        let buffer = Buffer.alloc(4);
        // Byte 0: Set Mask. Bit 0: style, Bit 1: Next Transition
        let setMask = 0;
        buffer.writeUInt8(data.me,1); 
        
        // Next Transition Style
        if('transition' in data){
            setMask = 1;
            buffer.writeUInt8(data.transition,2)
        } else {
            // may not need this due to mask
            buffer.writeUInt8(this.store.me[me].nextStyle, 2);
        }

        // Next Transition keyters
        if('nextTranstion' in data){
            setMask += 2;
            if(typeof data.nextTransition == 'number'){
                buffer.writeUInt8(data.nextTransition,2); 
            } else if (typeof data.nextTransition == 'object') {
                const current = this.store.me[me].nextKeyers;
                let next = 0;
                if('background' in data.nextTransition) {
                    if(data.nextTransition.background) next += 1;
                } else {
                    if(current[0]) next += 1;
                }
                if('keyer1' in data.nextTransition) {
                    if(data.nextTransition.keyer1) next += 2;
                } else {
                    if(current[1]) next += 2;
                }
                if('keyer2' in data.nextTransition) {
                    if(data.nextTransition.keyer2) next += 4;
                } else {
                    if(current[2]) next += 4;
                }
                if('keyer3' in data.nextTransition) {
                    if(data.nextTransition.keyer3) next += 88;
                } else {
                    if(current[3]) next += 8;
                }
                if('keyer4' in data.nextTransition) {
                    if(data.nextTransition.keyer4) next += 16;
                } else {
                    if(current[4]) next += 16;
                }
                
                buffer.writeUInt8(next,2); 
            } else {
                throw Exception("Wrong type passed to prepare setTransition");
            }
        }
        buffer.writeUInt8(setMask, 0);
        return buffer;
    }
}