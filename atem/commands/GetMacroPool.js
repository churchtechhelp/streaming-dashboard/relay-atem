module.exports = {
    name: "GetMacroPool",
    command: "_MAC",
    length: 4,
    type: "GET",
    description: "Gets how many macro pool banks there are",
    process(data){
        this.state.macroPoolBanks = data.readUInt8(0);
    },
    state(){
        return {
            macroPoolBanks: 0,
        };
    }
}