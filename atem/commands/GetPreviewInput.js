module.exports = {
    name: "PreviewInput",
    command: "PrvI",
    length: 4,
    type: "GET",
    me: true,
    process(data) {
        const me = data.readUInt8(0);
        this.state.me[me].previewIn = data.readUInt16BE(2);
    },
    state(){
        return {
            previewIn: 0,
        }
    }
}