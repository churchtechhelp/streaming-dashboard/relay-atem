module.exports = {
    name: 'Protocol',
    description: 'Gets the Firmware version (different from software control version)',
    type: 'GET',
    command: '_ver',
    length: 4, // Bytes
    version: '1.0',
    // No Max Version
    process(data) {
        // First two bytes are Major version
        this.state.firmwareVersion.major = data.readUInt16BE(0);
        // Next two bytes are the Minor Version
        this.state.firmwareVersion.minor = data.readUInt16BE(2);
    },
    state(){
        return {
            firmwareVersion: {
                major: 0,
                minor: 0,
            }
        }
    }
}