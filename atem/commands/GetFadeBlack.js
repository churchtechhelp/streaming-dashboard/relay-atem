module.exports = {
    name: "GetFadeToBlack",
    type: "GET",
    command: "FtbS",
    me: true,
    process(data){
        const me = data.readUInt8(0);
        this.state.me[me].ftb.black = data.readUInt8(1) == 1;
        this.state.me[me].ftb.transitioning = data.readUInt8(2) == 1;
        this.state.me[me].ftb.frames_left = data.readUInt8(3);
    },
    state(){
        return {
            // note: ignoring ME for now. I guess there can be two?
            ftb: {
                black: false,
                transitioning: false,
                frames_left: 0,
            }
        }
    }
}