module.exports = {
    name: "TransitionPosition",
    command: "TrPs",
    description: "Gets that position of the main transition slider",
    length: 8,
    type: "GET",
    me: true,
    process(data){
        const me = data.readUInt8(0);
        this.state.me[me].transitioning = data.readUInt8(1) == 1;
        this.state.me[me].transitionFrames = data.readUInt8(2);
        this.state.me[me].transitionPosition = data.readUInt16BE(4);
    },
    state(){
        return {
            transitioning: true,
            transitionFrames: 30, // 1 second. This is for the auto transition
            transitionPosition: 0,
        }
    }
}