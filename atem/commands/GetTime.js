module.exports = {
    name: "Time",
    type: "GET",
    command: "Time",
    description: "The switcher sent a time change",
    length: 8,
    process(data){
        this.state.time.hour = data.readInt8(0);
        this.state.time.minute = data.readInt8(1);
        this.state.time.second = data.readInt8(2);
        this.state.time.frame = data.readInt8(3);
        // Rest are unknown
    },
    state(){
        return {
            time: {
                hour: 0,
                minute: 0,
                second: 0,
                frame: 0,
            },
        }
    }
}