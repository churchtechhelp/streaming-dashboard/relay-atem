module.exports = {
    name: 'MultiviewInfo',
    command: '_MvC',
    type: "GET",
    length: 4,
    process(data){
        this.state.multiviews = data.readUInt8(0);
    },
    state(){
        return {
            multiviews: 0,
        }
    }
}