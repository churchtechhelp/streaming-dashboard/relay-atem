module.exports = {
    name: "InitComplete",
    type: "GET",
    command: "InCm",
    description: "Switcher Initialization is complete. Should be the last command after the set of info commands after connection",
    process(data){
        this.state.initialized = true;
        console.log("Initialization Complete");
    },
    state(){
        return {
            initialized: false,
        }
    }
}