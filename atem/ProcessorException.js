class ProcessorException extends Error {
    constructor(message){
        super(message);
        this.name = "ProcessorException"
    }
}

module.exports = ProcessorException;