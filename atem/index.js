const {EventEmitter} = require('events');
const path = require('path');
const fs  = require('fs');
const Socket = require('./socket.js');
const onChange = require('on-change');
const mDnsSd  = require('node-dns-sd');
const _ = require('lodash');
const electron = require('electron');
const commands = require('./commands/index.js');
const ProcessorException = require ('./ProcessorException.js')
const PreparerException = require ('./PreparerException.js')


class Atem extends EventEmitter {
    constructor(){
        super();

        // Load Config File
        const userDataPath = (electron.app || electron.remote.app).getPath('userData');
        this.configFilePath = path.join(userDataPath, 'atem.json');
        if(fs.existsSync(this.configFilePath)){
            this.config = JSON.parse(fs.readFileSync(this.configFilePath));
        } else {
            this.config = {"mode":"static","name":"","ip":"192.168.1.2"}
            fs.writeFileSync(this.configFilePath,JSON.stringify(this.config));
        }

        this.commands = {};
        this.commandPackets = {};
        this.loaded = false;
        this.connected = false;
        this.address = ""
        this.state = {
            me: [{},{}],
        };

        this.PreparerException = PreparerException;
        this.ProcessorException = ProcessorException;

        this.discovered = {};

        console.log("ATEM Config:", this.config)

        if(this.config.mode == 'static'){
            this.address = this.config.ip;
        } 
        console.log("Resolved ATEM IP to", this.address);
    }

    sendCommand(command,data){
        // sends command on socket
        console.log("number of preparers", Object.keys(this.packetPreparers).length);
        if(command in this.packetPreparers){
            console.log("Sending socket command",command);
            this.socket.sendCommand(this.commands[command].command,this.packetPreparers[command].call(this,data));
        } else {
            console.log("No handler for packet send command", command);
        }
    }

    recvCommand(command,data){
        //console.log("Received command",command);
        if(this.packetProcessors[command]) {
            this.packetProcessors[command].call(this,data);
        } else {
            console.log("no handler for packet command",command);
        }
    }

    getState(){
        return this.state;
    }

    isConnected(){
        return this.connected;
    }

    getStatus(){
        return {
            connected: this.connected,
            loaded: this.loaded,
            currentAddress: this.address,
            config: this.config,
        }
    }

    async discover(){
        console.log("Discovering");
        const devices = await mDnsSd.discover({
            name: '_blackmagic._tcp.local',
        });
        //console.log(JSON.stringify(devices, null, '  '));
        // Now we need to filter them before returning
        this.discovered = {};
        devices.forEach(device=>{
            if(!(device.modelName in this.discovered)){
                this.discovered[device.modelName] = {
                    fqdn: device.fqdn,
                    name: device.modelName
                };
            }
            if(device.address.startsWith('169.254')) this.discovered[device.modelName]['apipa'] = device.address;
            else this.discovered[device.modelName]['local'] = device.address;
        });
        this.updateAddress();
    }

    async load() {
        let me = {};
        
        // Clear arrays
        this.packetProcessors = {};
        this.packetPreparers = {};
        this.commands = {};
        this.state = {};

        for (const command of Object.values(commands)) {
            if(!command.type) {
                throw "Failed to import";
            }
            // Validate basic requirements
            if(!(command.name && typeof command.name == 'string')){
                console.log(file,"does not have a name");
                throw "Missing Name in file " + file;
            } 

            if(command.name in this.commands){
                throw "Duplicate Command Name: " + command.name
            }

            if(!(command.type && typeof command.type == 'string')){
                console.log(file,"does not have a type")
            }

            // Outgoing
            this.commands[command.name] = command;
            // Incoming packet decoding
            if(command.type == "GET" && command.process && typeof command.process == 'function'){
                if(command.command in this.packetProcessors){
                    throw "Duplicate Getter Command " + command.command;
                }
                this.packetProcessors[command.command] = command.process;
            }
            if(command.type == "SET" && command.prepare && typeof command.prepare == 'function'){
                if(command.command in this.packetPreparers){
                    throw "Duplicate Set Command " + command.command;
                }
                this.packetPreparers[command.name] = command.prepare;
            }
            // State Setup
            if(command.type == "GET" && command.state){
                // Parse State by adding all objects to the state.
                // TODO: Conflict Detection in state
                if(command.me){
                    // Currently a maximum of two ME's
                    me = _.merge(me, command.state());
                } else {
                    this.state = _.merge(this.state, command.state());
                }
            }
        }

        this.state.me = [me,me];

        this.defaultState = this.state;

        //console.log("Transitions",this.state.me[0].transitions);

        this.loaded = true;
    }

    async connect(){
        if(!this.loaded) return false;
        if(this.connected) {
            console.log("Already connected");
        }

        this.socket = new Socket(this.address);
        this.socket.on('message', (command,data) => this.recvCommand(command,data));
        this.socket.on('connect', () => this.onConnect());
        this.socket.on('disconnect', () => this.onDisconnect());

        await this.socket.connect();
    }

    onConnect(){
        // The switcher will automatically send all the right information.
        // We will just need to let the websocket clients know that we are connected and the state is good
        console.log("Connected to ATEM");
        this.emit("connected");
        this.connected = true;
        this.state.connected = true;

        // We also need to watch the state to efficiently send the changes
        this.state = onChange(this.state,(valuePath,value,lastValue)=>this.onStateChange(valuePath,value,lastValue))
    }

    async disconnect(){
        await this.socket.destroy();
        this.socket.removeAllListeners();
        this.onDisconnect();
    }

    onDisconnect(){
        // Let the websocket client know about the difference
        console.log("Disconnect from ATEM")
        this.emit("disconnected")
        this.connected = false;
        this.state.connected = false;
        this.socket = null;

        // Since there are no more changes, then we will stop listening to state changes.
        onChange.unsubscribe(this.state);
    }

    onStateChange(valuePath,value,lastValue){
        let changePacket = {};
        changePacket[valuePath] = value;
        this.emit('stateChanged',changePacket);
    }

    configure(key,value){
        this.config[key] = value;
        fs.writeFileSync(this.configFilePath,JSON.stringify(this.config));
        this.updateAddress();
    }

    updateAddress(){
        const oldAddress = this.address;
        if(this.config.mode == 'mdns'){
            if(this.config.name in this.discovered){
                if('local' in this.discovered[this.config.name]) {
                    this.address = this.discovered[this.config.name].local;
                } else if('apipa' in this.discovered[this.config.name]) {
                    this.address = this.discovered[this.config.name].apipa;
                }
            }
        } else if (this.config.mode == 'static'){
            this.address = this.config.ip
        }

        if(oldAddress != this.address){
            this.emit('ipChange');
        }
    }
}

module.exports = Atem 