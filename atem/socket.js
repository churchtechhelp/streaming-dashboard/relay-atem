const { createSocket } = require('dgram');
const { EventEmitter } = require('events');

// Flags are 5 bits
const FLAGS       = 0x1F;
const FLAG_ACKREQ = 0x01; // Please acknowledge
const FLAG_HELLO  = 0x02; // Hello - Send upon connection. Resets the Remote Packet ID and Session ID.
const FLAG_RESEND = 0x04; // This is a resend
const FLAG_REPEAT = 0x08; // Resend something. Probably only used with poor connection
const FLAG_ACKREP = 0x10; // Acknowledgement to packet id

function buf2hex(buffer) { // buffer is an ArrayBuffer
    return Array.prototype.map.call(new Uint8Array(buffer), x => ('00' + x.toString(16)).slice(-2)).join('');
}

class Socket extends EventEmitter {
    constructor(address){
        super()
        this.socket = createSocket('udp4');
        this.socket.bind();
        this.socket.on('message', (msg,info) => this.process(msg,info));
        
        // Connection Info/Status
        this.connected = false;
        this.waitingConnection = false;
        this.inError = false;
        this.port = 9910;
        this.address = address;

        // Packet Info
        this.lastPacketAt = 0;
        this.lastPacketTimer = null;
        this.lastRemotePacketId = 0;
        this.localPacketID = 0;
        this.sessionId = 0;
        this.packetLog = {};
        
    }

    // Process will take in the packet information and process it and emit the appropriate event.
    // Private function
    async process(msg,info) {
        if(this.inError) return;
        // msg is the buffer of the recieved packet
        // info is an object of the info (address,ip family,port,size) - not going to care about this for now
        // Set last packet
        this.lastPacketAt = Date.now();
        // BYTES 0-1:
        //  BITS: 0-4: Flags
        //       5-15: Packet Length
        // BYTES 2-3:   Session ID
        // 4 Uknown BYTES
        
        // Extract length (16 bits big endian minus the 5 flags)
        const length = msg.readUInt16BE(0) & 0x07ff;
        // Extract flags (first 5 bits of first byte)
        const flags = msg.readUInt8(0) >> 3;
        const sessionId = msg.readUInt16BE(2);
        this.sessionId = sessionId;
        const remotePacketId = msg.readUInt16BE(10);

        //console.log("Received a Packet. Length:",length,"Flags:",flags,"RemoteID:",remotePacketId, "sessionId:", sessionId);

        
        if(flags & FLAG_HELLO){
            // Received a hello packet
            if(this.connected) this.inError = true;
            //console.log("It is a hello packet");
            this.connected = true;
            this.waitingConnection = false;
            this.remotePacketId = remotePacketId;
            // now, send ack back
            this.sendAck(true);
            this.emit("connect");
            this.lastPacketTimer = setInterval(()=>{
                const timeSinceLastPacket = Date.now() - this.lastPacketAt;
                if(timeSinceLastPacket > 5000) {
                    this.connected = false;
                    clearInterval(this.lastPacketTimer);
                    this.emit("disconnect");
                }
            },1000);
        } else if(this.connected) {
            if(flags & FLAG_REPEAT) {
                // this is a retransmit request
                console.log("Retransmit request");
                this.retransmit(packet.readUInt16BE(6));
            }
            if(flags & FLAG_ACKREP){
                // This is an ack reply. Remove the packet from the packet log.
                if(this.packetLog[msg.readUInt16BE(4)]){
                    // TODO: Keep track of ack requests and delete packets since last ack too
                    delete this.packetLog[msg.readUInt16BE(4)];
                }
            }
            if(flags & FLAG_ACKREQ) {
                //console.log("Acknowledgement requested");
                // Ack request. Probably a command packet.
                // if packet is the next one (including if it hit max and wrapped around). The switcher does the same thing on the other side.
                if(((this.remotePacketId + 1) % 0x8000) == remotePacketId){ // 
                    this.remotePacketId = remotePacketId;

                    // If length is more than just the headers, than this is a command packet
                    if(length > 12) {
                        // Since there can be multiple commands in a packet, we need to keep processing until there are
                        // less bytes than the smallest command (8 bytes)

                        // Remove the packet's info
                        let commandPacket = msg.slice(12);
                        let commandLength = 12;
                        let command = "";
                        let data = {};

                        // process the commands in the packet
                        while(commandPacket.length > 8){
                            // Extract command and emit. Starts at byte 12.
                            // First 2 bytes are the acutal command length. Will use this to truncate packet
                            commandLength = commandPacket.readUInt16BE(0);
                            // Next 4 are the command
                            command = commandPacket.toString('ascii',4,8);
                            if(command == 'TWpP' || command == 'TDvP') {
                                console.log(command + " packet found. Length is:",commandLength);
                                console.log("Command Data as string:",commandPacket.toString('ascii',0,commandLength));
                                console.log("Command data raw:",commandPacket.slice(0,commandLength));
                            }
                            // Rest of command is the payload
                            data = commandPacket.slice(8,commandLength);
                            // Emit the command so it can be handled
                            this.emit("message",command, data);
                            // Trim off the length of the command
                            commandPacket = commandPacket.slice(commandLength);
                        }
                    } 
                } else {
                    //console.log("remote packet ID out of bounds. Got",remotePacketId, "expecting",((this.remotePacketId + 1) % 0x8000));
                }
                // For anything, ack:
                // NOTE: atem-connection library uses a timer to send acks 5ms afterwards OR ater 16th un-acked packet
                this.sendAck();
            }
            // Rest of packets, extract and emit command

            
        }
    }

    sendAck(force = false){
        //console.log("Acknowledging packet",this.remotePacketId,"for session",this.sessionId);
        const ackPacket = Buffer.alloc(12,0);
        ackPacket.writeUInt8((FLAG_ACKREP << 3), 0);
        ackPacket.writeUInt8(12,1);
        ackPacket.writeUInt16BE(this.sessionId,2);
        ackPacket.writeUInt16BE(this.remotePacketId,4); // last packet ID
        this.socket.send(ackPacket,this.port,this.address);
    }

    sendCommand(command,data){
        if(!this.connected) return;
        // BYTES 0-1:
        //  BITS: 0-4: Flags
        //       5-15: Packet Length (will need to get highest 3 bits to add to byte with flags)
        // BYTES 2-3:  Session ID
        // BYTES 4-5:  Remote Packet ID (for ack)
        // Bytes 6-9:  ???
        // Bytes 10-11:Local Packet ID (from local counter): Only do if not a Hello/Repeat/ACKREP packet
        // Bytes -1: Packet ID (6 bits?)??
        // Form Headers
        const currentPacketId = this.localPacketID++;
        if(this.localPacketID >= 0x8000 ) this.localPacketID = 0;
        console.log("Packet id is",currentPacketId);
        let packet = Buffer.alloc(20 + data.length, 0);
        packet.writeUInt16BE((FLAG_ACKREQ << 11) | (data.length + 20),0); // Ack Request flag and length
        packet.writeUInt16BE(this.sessionId,2);
        console.log("Session ID",this.sessionId);
        // Skip remotePacketID, Unknown bytes
        packet.writeUInt16BE(currentPacketId,10);
        packet.writeUInt16BE(data.length + 8, 12); // Not sure what this is
        packet.write(command, 16, 4); // byte 16 is the command string
        Buffer.from(data).copy(packet,20); // Take the data array and copy into the buffer. If it already is a buffer, then we can omit the Buffer.from part
        console.log("Sending packet",buf2hex(packet), "to " + this.address + ":" + this.port);
        this.socket.send(packet,this.port,this.address);
        this.packetLog[currentPacketId] = packet; // atem-connection also keeps track of last sent time and resent count
    }

    retransmit(packetId) {
        let packet = this.packetLog[packetId];
        let flags = packet.readUInt8(0);
        packet.writeUInt8(flags | (FLAG_RESEND << 3));
        this.socket.send(packet,this.port, this.address);
    }

    async connect(){
        // Actually Connect the Client socket to the ATEM

        //QByteArray datagram = createCommandHeader(Cmd_HelloPacket, 8, m_currentUid, 0x0);
        //datagram.append(QByteArray::fromHex("0100000000000000"));

        // Send HELLO packet
        let packet = Buffer.from([
            0x10, // Hello
            0x14, // packet length = 20
            0x53, 0xab, // "RANDOM" session ID
            0x00, 0x00, // Remote packet ID (Not an ack so 0)
            0x00, 0x00, 0x00, // Zeros (unknown)
            0x3a, // Random number?
            0x00, 0x00, // Local Packet ID = 0 since first packet
            0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 // 0x0100000000000000 as data
        ]);
        
        this.waitingConnection = true;

        this.localPacketID = 1;

        this.socket.send(packet,this.port,this.address);
    }

    async disconnect(){
        this.connected = false;
    }

    async destroy(){
        await this.disconnect()
        this.socket.close();
    }
}

module.exports = Socket;