const { ipcRenderer } = require('electron')

ipcRenderer.on('update', (event, arg) => {
    const status = JSON.parse(arg);
    const toggleBtn = document.getElementById('atemToggle');
    // ATEM Status
    if(!status.atem.loaded) {
        document.getElementById('atemstatus').innerHTML = "Loading";
        toggleBtn.classList.remove('btn-danger','btn-primary');
        toggleBtn.classList.add('btn-warning');
    } else if(status.atem.connected) {
        document.getElementById('atemstatus').innerHTML = "Connected";
        toggleBtn.innerHTML = 'Disconnect';
        toggleBtn.style.display = 'inline';
        toggleBtn.classList.remove('btn-primary');
        toggleBtn.classList.add('btn-danger');
    } else {
        document.getElementById('atemstatus').innerHTML = "Disconnected";
        toggleBtn.innerHTML = 'Connect';
        toggleBtn.style.display = 'inline';
        toggleBtn.classList.remove('btn-danger');
        toggleBtn.classList.add('btn-primary');
    }
    // ATEM Current IP
    document.getElementById('atemip').innerHTML = status.atem.currentAddress;
    console.log("Config:",status.atem.config);
    // ATEM Discovery Mode
    document.getElementById('atemMode').value = status.atem.config.mode;
    if(document.getElementById('atemMode').value == 'mdns') {
        document.getElementById('atemIPSet').parentElement.style.display = 'none';
        document.getElementById('atemNamePicker').parentElement.parentElement.style.display = 'block';
    } else {
        document.getElementById('atemIPSet').parentElement.style.display = 'block';
        document.getElementById('atemNamePicker').parentElement.parentElement.style.display = 'none';
    }
    // ATEM Static IP
    document.getElementById('atemIPSet').value = status.atem.config.ip;
});
ipcRenderer.on('connected', (evt,arg)=>{
    arg = JSON.parse(arg);
    console.log("Connected");
    const toggleBtn = document.getElementById('atemToggle');
    document.getElementById('atemstatus').innerHTML = "Connected";
    toggleBtn.innerHTML = 'Disconnect';
    toggleBtn.style.display = 'inline';
    toggleBtn.classList.remove('btn-primary');
    toggleBtn.classList.add('btn-danger');
    document.getElementById('atemProduct').innerHTML = arg.product;
    document.getElementById('atemInfo').elementList.remove('d-none');
});
ipcRenderer.on('disconnected', (evt,arg)=>{
    console.log("disconnected");
    const toggleBtn = document.getElementById('atemToggle');
    document.getElementById('atemstatus').innerHTML = "Disconnected";
    toggleBtn.innerHTML = 'Connect';
    toggleBtn.style.display = 'inline';
    toggleBtn.classList.remove('btn-danger');
    toggleBtn.classList.add('btn-primary');
    document.getElementById('atemProduct').innerHTML = "";
    document.getElementById('atemInfo').elementList.add('d-none');
});
ipcRenderer.on('discovered', (evt,arg)=>{
    arg = JSON.parse(arg);
    console.log("Discovered devices", arg);
    const picker = document.getElementById('atemNamePicker');
    picker.innerHTML = "";
    let devices = {};
    if(Object.keys(arg.discovered).length > 0){
        devices = arg.discovered;
    } else {
        // empty
        return;
    }
    let optionSelected = false;
    const blankOption = document.createElement('option');
    blankOption.text = "None";
    blankOption.disabled = true;
    blankOption.value = "";
    if(arg.current === undefined || arg.current === "") {
        arg.current == ""
        optionSelected = true;
    }
    picker.add(blankOption);
    let option;
    for (const [name, value] of Object.entries(devices)) {
        option = document.createElement('option');
        option.text = name;
        option.value = name;
        if(name == arg.current){
            optionSelected = true;
        }
        picker.add(option);
    }
    if(!optionSelected){
        option = document.createElement('option');
        option.text = arg.current + " (Not Found)";
        option.value = arg.current;
        picker.add(option);
    }
    console.log("Currently selected name is ", arg.current);
    picker.value = arg.current;
})
ipcRenderer.on('currentIp', function(e,a){
    document.getElementById('atemip').innerHTML = a;
})
ipcRenderer.on('autolaunch', function (evt, enabled){
    document.getElementById('autolaunch').disabled = false;
    if(enabled){
        document.getElementById('autolaunch').checked = 'checked';
        console.log("Auto launch enabled");
    } else {
        document.getElementById('autolaunch').checked = false;
        console.log("Auto launch disabled");
    }
});
ipcRenderer.send('init');

document.getElementById('atemMode').addEventListener('change', function(e){
    console.log('Discovery Mode has been changed');
    ipcRenderer.send('mode',e.target.value);
    if(e.target.value == 'mdns') {
        document.getElementById('atemIPSet').parentElement.style.display = 'none';
        document.getElementById('atemNamePicker').parentElement.parentElement.style.display = 'block';
    } else {
        document.getElementById('atemIPSet').parentElement.style.display = 'block';
        document.getElementById('atemNamePicker').parentElement.parentElement.style.display = 'none';
    }
});

document.getElementById('atemIPSet').addEventListener('change', function(e){
    console.log('IP has been changed');
    ipcRenderer.send('ip',e.target.value);
});
document.getElementById('atemNamePicker').addEventListener('change', function(e){
    console.log('name has been changed');
    ipcRenderer.send('name',e.target.value);
});

document.getElementById('atemToggle').addEventListener('click', e => {
    ipcRenderer.send('toggle');
});

document.getElementById('rediscover').addEventListener('click', e => {
    ipcRenderer.send('discover');
});

document.getElementById('autolaunch').addEventListener('click', e => {
    ipcRenderer.send('autolaunch');
})
