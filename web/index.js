const {EventEmitter} = require('events');
const WebSocket = require('ws');
const express = require('express');
const https = require('https');
const fs = require('fs');
const os = require('os');
const path = require('path');
const selfsigned = require('selfsigned');
const electron = require('electron');

class Web extends EventEmitter {
    constructor(){
        super();
        // Load Config File
        const userDataPath = (electron.app || electron.remote.app).getPath('userData');
        console.log("User path:",userDataPath);
        const configFilePath = path.join(userDataPath, 'web.json');
        if(fs.existsSync(configFilePath)){
            this.config = JSON.parse(fs.readFileSync(configFilePath));
        } else {
            // Config does not exist
            this.config = {
                "privateKey": "key.pem",
                "certificate": "cert.crt",
                "port": 4455,
            };
            fs.writeFileSync(configFilePath, JSON.stringify(this.config));
        }

        // Process config file
        this.keyPath = path.join(userDataPath, this.config.privateKey);
        this.certPath = path.join(userDataPath, this.config.certificate);  // TODO: Use path to resolve properly.
        this.port = this.config.port;

        // TODO: If cert/key does not exist, create them
        if(!fs.existsSync(this.keyPath)){
            const attrs = [{ name: 'commonName', value: os.hostname() }];
            const pems = selfsigned.generate(attrs, { days: 365 });
            // output: pems.private, pems.cert - need to export them to configured location
            fs.writeFileSync(this.keyPath, pems.private);
            fs.writeFileSync(this.certPath, pems.cert);
        }

        this.app = express();
        this.app.use(express.static('public'));
        this.server = https.createServer({
            key: fs.readFileSync(this.keyPath),
            cert: fs.readFileSync(this.certPath)
        },this.app);
        this.server.listen(this.port);

        this.ws = new WebSocket.Server({ server: this.server });

        this.clients = this.ws.clients;

        const self = this;

        this.ws.on('connection', function open(client,request) {
            console.log("WS Client Connected");
            self.emit('wsConnect', client);
            
            // Websocket Command Received message hook
            client.on('message', function incoming(message) {
                console.log("WS Message received",message)
                self.emit('message',message);
                // convert from serialized string to object
            });
        });

        this.ws.on('close', function close(){
            this.emit('disconnection');
        });
    }

    configure(key,value){
        // Options: Port, Certificate
        this.config[key] = value;
        fs.writeFileSync('../config/web.json', JSON.stringify(this.config));
        
        // Remember to restart server after changing!
    }
}

module.exports = Web;