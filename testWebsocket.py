import websocket, json
from datetime import datetime
from pprint import PrettyPrinter
try:
    import thread
except ImportError:
    import _thread as thread
import time

pp = PrettyPrinter(indent=4)

def on_message(ws, message):
    print(datetime.now())
    pp.pprint(json.loads(message))

def on_error(ws, error):
    print(error)

def on_close(ws):
    print("### closed ###")

if __name__ == "__main__":
    websocket.enableTrace(True)
    ws = websocket.WebSocketApp("ws://localhost:4455/",
                              on_message = on_message,
                              on_error = on_error,
                              on_close = on_close)
    ws.run_forever()