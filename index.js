const Atem = require('./atem/index.js');
const Web = require('./web/index.js');
const { app, BrowserWindow, ipcMain, Menu } = require('electron')
const AutoLaunch = require('auto-launch');

// Auto-Launcher
const autoLauncher = new AutoLaunch({
    name: 'Streaming_Dashboard_ATEM_Relay'
});

const atem = new Atem();
const webServer = new Web();
const status = {
    status: 'Initializing',
    clients: 0,
}

/***********************************
 * Web Socket Server Events
 ***********************************/

var reconnectInterval = null;
var manualDisconnect = false;

// Websocket Opened: Send initial data
webServer.on('wsConnect', function(client) {
    status.clients += 1;
    if(atem.isConnected()) {
        let packet = {
            event: "connected",
            value: atem.getState(),
        };
        console.log("Sending current state");
        client.send(JSON.stringify(packet));
    } else {
        console.log("Atem not connected");
    }
});

webServer.on('disconnection', function(){
    status.clients -= 1;
});

webServer.on('message', message => {
    const command = JSON.parse(message);
    atem.sendCommand(command.command, command.data);
});


/***********************************
 * ATEM Events
 ***********************************/

// Atem Connected
atem.on('connected', () => {
    if(reconnectInterval){
        clearInterval(reconnectInterval);
    }
    status.status = 'Connected';

    let changePacket = {
        event: "connected",
        value: atem.getState(),
    };

    webServer.clients.forEach((client)=> {
        client.send(JSON.stringify(changePacket));
    });
});

atem.on('disconnected', function() {
    let changePacket = {
        event: "disconnected",
    };
    status.status = 'Disconnected';

    webServer.clients.forEach((client)=> {
        client.send(JSON.stringify(changePacket));
    });

    if(!manualDisconnect){
        reconnectInterval = setInterval(function(){
            console.log("Attempting reconnect");
            atem.connect();
        },5000);
    }
})
 
// Atem State change: Send websocket update
// TODO: Only send difference. Technically only have to look for one?
atem.on('stateChanged', function(change) {
    //console.log("State Change:",change);
    let changePacket = {
        event: "stateChanged",
        value: change,
    }

    // Convert to string and send to all clients
    webServer.clients.forEach((client)=> {
        client.send(JSON.stringify(changePacket));
    })
});

/***********************************
 * GUI Setup
 ***********************************/

function createWindow () {
    const win = new BrowserWindow({
        width: 800,
        height: 600,
        webPreferences: {
            nodeIntegration: true
        }
    });
    
    win.loadFile('gui/index.html');
}
    
app.whenReady().then(createWindow);
Menu.setApplicationMenu(null);

app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
        app.quit();
    }
});

app.on('activate', () => {
        if (BrowserWindow.getAllWindows().length === 0) {
            createWindow();
        }
});

ipcMain.on('init', async function(evt,arg) {
    const statusMsg = JSON.stringify({
        atem: atem.getStatus(),
    });
    // Immediate status
    evt.sender.send('update',statusMsg);
    atem.on('connected',function(){
        evt.sender.send('connected',JSON.stringify({
            product: atem.state.product,
        }));
    });
    atem.on('disconnected',function(){
        evt.sender.send('disconnected');
    });
    atem.on('ipChange', function(){
        evt.sender.send('currentIp', atem.address);
    });
    await atem.discover();
    evt.sender.send('discovered',JSON.stringify({discovered: atem.discovered, current: atem.config.name}));
    if(atem.config.mode == 'mdns' && atem.config.name !== undefined && atem.config.name !== '' && !atem.connected && !manualDisconnect){
        reconnectInterval = setInterval(function(){
            console.log("Attempting reconnect");
            atem.connect();
        },5000);
        atem.connect();
    }

    // Auto launch initial status
    autoLauncher.isEnabled().then(function(isEnabled) {
        evt.sender.send('autolaunch',isEnabled);
    });
});

ipcMain.on('toggle', function(evt){
    if(atem.connected){
        console.log("Disconnecting manually");
        manualDisconnect = true;
        atem.disconnect();
    } else {
        console.log("Connecting manually");
        manualDisconnect = false;
        atem.connect();
    }
});
ipcMain.on('discover', async function(evt){
    await atem.discover();
    evt.sender.send('discovered',JSON.stringify({local: atem.localDiscovered, direct: atem.apippaDiscovered, current: atem.config.name}));
});

// Config changes
ipcMain.on('ip', function(evt,arg){
    atem.configure('ip',arg);
});
ipcMain.on('name', function(evt,arg){
    atem.configure('name',arg);
});
ipcMain.on('mode', function(evt,arg){
    atem.configure('mode',arg);
});

// Autolaunch toggle
ipcMain.on('autolaunch', function (evt, arg){
    autoLauncher.isEnabled().then(function(isEnabled) {
        console.log("Toggling Auto Launcher")
        if(isEnabled){
            autoLauncher.disable().then(function(){
                console.log("Auto Launcher Disabled");
                evt.sender.send('autolaunch', false);
            });
        } else {
            autoLauncher.enable().then(function(){
                console.log("Auto Launcher Enabled");
                evt.sender.send('autolaunch', true);
            });
        }
    });
});

/***********************************
 * Auto connect
 ***********************************/
( async () => {
    await atem.load();
    await atem.connect();
})();
